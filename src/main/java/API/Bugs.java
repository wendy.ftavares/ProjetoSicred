package API;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.junit.BeforeClass;
import org.junit.Test;

public class Bugs {
	
	//As requisições abaixo apresentaram o status code 200 OK dentro do log, mas o código correto seria o 201 OK, como descrito na documentação do desafio. 
	//Ao fazer os testes manuais utilizando a ferramenta Postman, os status code apresentados foram 403 Forbidden quando fiz a requisição de Login (POST /auth/login) e 404 Not Found (POST /products/add) para a criação de produto. 
	//Talvez seja algum tipo de bug que a API está apresentando, ou, se não for, o ideal é que a documentação da API seja atualizada com novos dados ou colocando os códigos corretos.
	
	
	@BeforeClass
	public static void setup() {
		baseURI = "https://dummyjson.com";
	}
	
	@Test
	public void POSTInauthlogin() {
		
	given()
		.log().all()
		.contentType("application/json")
		.body("{\"username\": \"kminchelle\", \"password\": \"0lelplR\"}")
	.when()
		.post("/auth/login")
	.then()
		.log().all()
		.statusCode(201);
	
}
	
	@Test
	public void POSTproductsadd() {
		
	given()
		.log().all()
		.contentType("application/json")
		.body("{\"title\": \"Perfume Oil\", \"description\": \"Mega Discount, Impression of A...\", \"price\": 13, \"discountPercentage\": 8.4, \"rating\": 4.26, \"stock\": 65, \"brand\": \"Impression of Acqua Di Gio\", \"category\": \"fragrances\", \"thumbnail\": \"https://i.dummyjson.com/data/products/11/thumnail.jpg\"}")
	.when()
		.post("/products/add")
	.then()
		.log().all()
		.statusCode(201);
	
}
		
}
