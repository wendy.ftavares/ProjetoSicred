package API;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class Desafio {
	
	@BeforeClass
	public static void setup() {
		baseURI = "https://dummyjson.com";
	}
	
	@Test
	public void GETtest() {
		
	given()
		.log().all()
	.when()
		.get("/test")
	.then()
		.statusCode(200)
		.log().all()
		.body("status", is("ok"));
	
}
	
	@Test
	public void GETusersTerry() {
		
	given()
		.log().all()
	.when()
		.get("/users/1")
	.then()
		.statusCode(200)
		.log().all()
		.body("id", is(1))
		.body("username", is("atuny0"))
		.body("password", is("9uQFF1Lh"));
}
	
	@Test
	public void GETusersSheldon() {
		
	given()
		.log().all()
	.when()
		.get("/users/2")
	.then()
		.log().all()
		.statusCode(200)
		.body("id", is(2))
		.body("username", is("hbingley1"))
		.body("password", is("CQutx25i8r"));
	
}
	
	@Test
	public void POSTInauthloginvalid() {
		
	given()
		.log().all()
		.contentType("application/json")
		.body("{\"username\": \"kminchlle\", \"password\": \"0lelplR\"}")
	.when()
		.post("/auth/login")
	.then()
		.log().all()
		.statusCode(400)
		.body("message", is("Invalid credentials"));
}
	
	@Test
	public void GETauthproducts401() {
		
	int token = 12345;
	given()
		.log().all()
		.contentType("application/json")
		.header("Authorization", "Bearer " + token)
	.when()
		.get("/auth/products")
	.then()
		.log().all()
		.statusCode(401)
		.body("name", is("JsonWebTokenError"))
		.body("message", is("Invalid/Expired Token!"));
}
	
	@Test
	public void GETauthproducts403() {
		
	given()
		.log().all()
		.contentType("application/json")
	.when()
		.get("/auth/products")
	.then()
		.log().all()
		.statusCode(403)
		.body("message", is("Authentication Problem"));
}
	
	@Test
	public void GETproductsList() {
		
	given()
		.log().all()
	.when()
		.get("/products")
	.then()
		.log().all()
		.statusCode(200)
		.body("products", hasSize(30))
		.body("products.id[0]", is(1))
		.body("products.title[0]", is("iPhone 9"))
		.body("products.price[0]", is(549))
		.body("products.id[29]", is(30))
		.body("products.title[29]", is("Key Holder"))
		.body("products.price[29]", is(30))
		.body("total", is(100))
		.body("limit", is(30));
	
}
	
	@Test
	public void GETproducts200() {
		
	given()
		.log().all()
	.when()
		.get("/products/1")
	.then()
		.log().all()	
		.statusCode(200)
		.body("id", is(1))
		.body("title", is("iPhone 9"))
		.body("price", is(549));
	
	}
	
	@Test
	public void GETproducts404() {
		
	given()
		.log().all()
	.when()
		.get("/products/0")
	.then()
	    .log().all()
		.statusCode(404)
		.body("message", is("Product with id '0' not found"));
	
}
	
}