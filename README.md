# DESAFIO SICREDI

## Objetivos
o objetivo desse projeto e garantir que a API esteja funcionando corretamente de acordo com a documentação apresentada pelo Sicred.

## Índice

- [Requisitos](#requisitos)
- [Instalação](#instalação)
- [Uso](#uso)
- [Contribuição](#contribuição)

## Requisitos

- Java JDK 
- Eclipse IDE
- JUnit
- Rest Assured
- Conta do GitLab

## Instalação

- Após a instalação dos programas listadados acima, faça o download do projeto no Gitlab e importe o projeto maven dentro do Eclipse IDE

*IMPORTANTE* : Caso a library do Java seja abaixo da versão SE-1.8 é necessário mudar a library do java manualmente no caminho Propriedades/Java Code Path/Libraries, após acessar o caminho exclua a library obsoleta e adcione uma nova JRE System Library podendo ser a Default ou no meu caso escolhendo a Java SE-1.8 na opção Execution environment.

- Confira se as dependências do Rest Assured e do Junit dentro do arquivo POM

```java
<dependencies>
		<dependency>
			<groupId>io.rest-assured</groupId>
			<artifactId>rest-assured</artifactId>
			<version>5.3.2</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.13.2</version>
		</dependency>
	</dependencies>
```

- Após salvar já conseguimos fazer os casos de testes dentro da API

## Uso

- Rode os testes como JUnit Test

## Contribuição

As requisições de POST /auth/login e a de POST /auth/login apresentaram o status code 200 OK dentro do log, mas o código correto seria o 201 OK, como descrito na documentação do desafio. 

Ao fazer os testes manuais utilizando a ferramenta Postman, os status code apresentados foram 403 Forbidden quando fiz a requisição de Login (POST /auth/login) e 404 Not Found (POST /products/add) para a criação de produto. 

Talvez seja algum tipo de bug que a API está apresentando, ou, se não for, o ideal é que a documentação da API seja atualizada com novos dados ou colocando os códigos corretos.

Em relação ao cenário 200OK da API GET/auth/products não  ficou muito explícito aonde eu poderia gerar o token para eu fazer teste de sucesso, ja que na maioria das vezes é necessário um cadastro para que a key da api seja gerada então por esse motivo esse foi o único cenário que ficou faltando dos 3 que estão presentes na API